#include <iostream>
#include <fstream>
#include <string>
#include <cmath>



// Tests para la funcion F

/* Test para multiples alfas usando criterio de parada cantidad de iteraciones */

void guardarTest(double* resultados, double* alfas, double* tiempos, int n, const char* nombre){
	
	ofstream salida;
	salida.open(nombre);
	salida << n << endl;
	for(int i = 0; i < n; i++){
		salida << resultados[i] << " ";
	}
	salida << endl;
	for(int i = 0; i < n; i++){
		salida << alfas[i] << " ";
	}
	salida << endl;
	for(int i = 0; i < n; i++){
		salida << tiempos[i] << " ";
	}
	salida << endl;
	salida.close();
}


void test_alfas_multiples_iteraciones_f_newton(double alfa, double x0){
	int i = 0;
	double resultados[20000];
	double vectorAlphas[20000];
	double vectorTiempos[20000];	
	criterio = "i";
	
	while (alfa < 10000) {	
		vectorAlphas[i] = alfa;
		gettimeofday(&t1,NULL);
		resultados[i] = newton_f(alfa,x0);
		gettimeofday(&t2, NULL);
		elapsedTime = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
		//elapsedTime += t2.tv_usec - t1.tv_usec;
		vectorTiempos[i] = elapsedTime;
		alfa = alfa +0.5;
		i++;
	}
	guardarTest(resultados,vectorAlphas,vectorTiempos,20000,"funcion_f_newton_alfas_multiples.txt");
}

void guardarArregloNewton(double resultado, const char* nombre,double x0, const char* criterio, double valorParada, int iteracionesUsadas, double tiempo){
	
	ofstream salida;
	
	salida.open(nombre);
	salida << "Alfa estimado: ";
	salida << resultado << endl;

	salida << "Criterio: ";
	salida << criterio << endl;

	salida << "Valor Parada: ";
	salida << valorParada << endl;

	salida << "Iteraciones consumidas: ";
	salida << iteracionesUsadas << endl;

	salida << "Tiempo: ";
	salida << tiempo << endl;

	salida << endl;
	salida.close();
	
}

void test_f(double alfa, double x0){
	
	gettimeofday(&t1,NULL);
	double resultado = newton_f(alfa,x0);
	gettimeofday(&t2, NULL);
	elapsedTime = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
//	elapsedTime += (t2.tv_usec - t1.tv_usec)/1000.0;

	if(strcmp(criterio, "i") ==  0){
		double iter = cant_iteraciones;
		guardarArregloNewton(resultado, "test_f_iteraciones.txt",x0, criterio, iter, cant_iteraciones, elapsedTime);
	}

	if(strcmp(criterio, "a") ==  0){
		guardarArregloNewton(resultado, "test_f_error_absoluto.txt",x0, criterio, epsilon, cant_iteraciones, elapsedTime);
	}
	if(strcmp(criterio, "d") ==  0){
		guardarArregloNewton(resultado, "test_f_diferencia_xn1_xn.txt",x0, criterio, epsilon, cant_iteraciones, elapsedTime);
	}
}




// Tests para la funcion E

void test_e_newton(double alfa, double x0){
	
	gettimeofday(&t1,NULL);
	double resultado = newton_e(alfa,x0);
	gettimeofday(&t2, NULL);
	elapsedTime = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
	//elapsedTime += (t2.tv_usec - t1.tv_usec)*1000.0;

	if(strcmp(criterio, "i") ==  0){

	double iter = cant_iteraciones;

	guardarArregloNewton(resultado, "test_e_newton_iteraciones.txt",x0, criterio, iter, cant_iteraciones,elapsedTime);
	}

	if(strcmp(criterio, "a") ==  0){
	guardarArregloNewton(resultado, "test_e_newton_error_absoluto.txt",x0, criterio, epsilon, cant_iteraciones, elapsedTime);
	}

	if(strcmp(criterio, "d") ==  0){
	guardarArregloNewton(resultado, "test_e_newton_diferencia_xn1_xn.txt",x0, criterio, epsilon, cant_iteraciones, elapsedTime);
	}
}

void guardarArregloSecante(double resultado, const char* nombre, double x0, double x1, const char* criterio, double valorParada, int iteracionesUsadas, double tiempo){
	
	ofstream salida;
	
	salida.open(nombre);

	salida << "X0: ";
	salida << x0 << endl;

	salida << "X1: ";
	salida << x1 << endl;

	salida << "Alfa estimado: ";
	salida << resultado << endl;

	salida << "Criterio: ";
	salida << criterio << endl;

	salida << "Valor Parada: ";
	salida << valorParada << endl;

	salida << "Iteraciones consumidas: ";
	salida << iteracionesUsadas << endl;

	salida << "Tiempo: ";
	salida << tiempo << endl;

	salida << endl;
	salida.close();
	
}


void test_e_secante(double alfa, double x0, double x1){
	
	gettimeofday(&t1,NULL);
	double resultado = secante_e(alfa,x0,x1);
	gettimeofday(&t2, NULL);
	elapsedTime = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
	//elapsedTime += (t2.tv_usec - t1.tv_usec)*1000.0;

	if(strcmp(criterio, "i") ==  0){

	double iter = cant_iteraciones;
	guardarArregloSecante(resultado, "test_e_secante_iteraciones.txt",x0, x1, criterio, iter, cant_iteraciones, elapsedTime);
	}
	if(strcmp(criterio, "a") ==  0){
	guardarArregloSecante(resultado, "test_e_secante_error_absoluto.txt",x0, x1, criterio, epsilon, cant_iteraciones, elapsedTime);
	}

	if(strcmp(criterio, "d") ==  0){
	guardarArregloSecante(resultado, "test_e_secante_diferencia_xn1_xn.txt",x0, x1, criterio, epsilon, cant_iteraciones, elapsedTime);
	}
}



/* Test para multiples alfas usando criterio de parada cantidad de iteraciones */
void test_alfas_multiples_iteraciones_e_newton(double alfa, double x0){
	int i = 0;
	double resultados[1600];
	double vectorAlphas[1600];
	double vectorCeros[1600];
	
	criterio = "d";
	cant_iteraciones = 8;
	epsilon = 0.001;
	
	while (i < 1600) {	
		vectorAlphas[i] = alfa;
		vectorCeros[i] = 1/(sqrt(alfa));
		//~ gettimeofday(&t1,NULL);
		resultados[i] = newton_e(alfa,x0);
		//~ gettimeofday(&t2, NULL);
		//~ elapsedTime = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
		//~ vectorTiempos[i] = elapsedTime;
		alfa = alfa + 0.01;
		i++;
	}
	guardarTest(resultados,vectorAlphas,vectorCeros,i,"funcion_e_newton_alfas_multiples_d.txt");
}

void test_alfas_multiples_iteraciones_e_secante(double alfa, double x0, double x1){
	int i = 0;
	double resultados[1600];
	double vectorAlphas[1600];
	double vectorCeros[1600];
	
	criterio = "d";
	cant_iteraciones = 10;
	epsilon = 0.0001;
	
	while (i < 1600) {	
		vectorAlphas[i] = alfa;
		vectorCeros[i] = 1/(sqrt(alfa));
		//~ gettimeofday(&t1,NULL);
		resultados[i] = secante_e(alfa,x0,x1);
		//~ gettimeofday(&t2, NULL);
		//~ elapsedTime = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
		//~ vectorTiempos[i] = elapsedTime;
		alfa = alfa +0.01;
		i++;
	}
	guardarTest(resultados,vectorAlphas,vectorCeros,i,"funcion_e_secante_alfas_multiples_d.txt");
}


void test_convergencia_e_newton(){
	
	int i;
	double alfa = 0.25;
	double x0s[8] = {0.1, 0.2, 0.5, 2, 4, 8, 20, 1000};
	double xns[8][9];

	criterio = "i";
	cant_iteraciones = 1;

	for (int j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];		//x0 en la primera posicion de xns
		
		for(i = 1; i < 9; i++){
		
			cout << "Iteracion " << j << "." << i << "..." << endl;
			xns[j][i] = newton_e(alfa,xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,9,"test_convergencia_e_newton_alfa_025.txt");
	
	alfa = 0.5;
	for (int j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];		//x0 en la primera posicion de xns
		
		for(i = 1; i < 9; i++){
		
			cout << "Iteracion " << j << "." << i << "..." << endl;
			xns[j][i] = newton_e(alfa,xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,9,"test_convergencia_e_newton_alfa_05.txt");
	
	alfa = 4;
	for (int j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];		//x0 en la primera posicion de xns
		
		for(i = 1; i < 9; i++){
		
			cout << "Iteracion " << j << "." << i << "..." << endl;
			xns[j][i] = newton_e(alfa,xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,9,"test_convergencia_e_newton_alfa_4.txt");
	
	alfa = 16;
	for (int j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];		//x0 en la primera posicion de xns
		
		for(i = 1; i < 9; i++){
		
			cout << "Iteracion " << j << "." << i << "..." << endl;
			xns[j][i] = newton_e(alfa,xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,9,"test_convergencia_e_newton_alfa_16.txt");
}

void test_convergencia_e_secante(){
	
	int i,j;
	double alfa = 0.25;
	double x0s[8] = {0.1, 0.2, 0.5, 2, 4, 8, 20, 1000};
	double x1s[8] = {0.15, 0.1, 0.4, 4, 5, 7, 17, 980};
	double xns[8][12];

	criterio = "i";
	cant_iteraciones = 1;

	for (j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];			//x0 en la primera posicion de xns
		xns[j][1] = x1s[j];			//x1 en la segunda posicion de xns
		
		for(i = 2; i < 12; i++){
		
			cout << "Iteracion " << j << "." << i-1 << "..." << endl;
			xns[j][i] = secante_e(alfa,xns[j][i-2],xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,12,"test_convergencia_e_secante_alfa_025.txt");
	
	alfa = 0.5;
	for (j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];			//x0 en la primera posicion de xns
		xns[j][1] = x1s[j];			//x1 en la segunda posicion de xns
		
		for(i = 2; i < 12; i++){
		
			cout << "Iteracion " << j << "." << i-1 << "..." << endl;
			xns[j][i] = secante_e(alfa,xns[j][i-2],xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,12,"test_convergencia_e_secante_alfa_05.txt");
	
	alfa = 4;
	for (j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];			//x0 en la primera posicion de xns
		xns[j][1] = x1s[j];			//x1 en la segunda posicion de xns
		
		for(i = 2; i < 12; i++){
		
			cout << "Iteracion " << j << "." << i-1 << "..." << endl;
			xns[j][i] = secante_e(alfa,xns[j][i-2],xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,12,"test_convergencia_e_secante_alfa_4.txt");
	
	alfa = 16;
	for (j = 7; j >= 0; j--){
		
		xns[j][0] = x0s[j];			//x0 en la primera posicion de xns
		xns[j][1] = x1s[j];			//x1 en la segunda posicion de xns
		
		for(i = 2; i < 12; i++){
		
			cout << "Iteracion " << j << "." << i-1 << "..." << endl;
			xns[j][i] = secante_e(alfa,xns[j][i-2],xns[j][i-1]);
		}
	}
	
	guardarMatriz(*xns,8,12,"test_convergencia_e_secante_alfa_16.txt");
}

void test_iteraciones_e_newton(){

	int j;
	double iteraciones[8] = {20, 15, 10, 8, 5, 4, 3, 2};
	double tiempos[8];
	double resultados[8];
	
	double x0 = 0.1;
	double alfa = 4;
	criterio = "i";
	

	for (j = 0; j < 8; j++){
		
		cant_iteraciones = iteraciones[j];
		gettimeofday(&t1,NULL);
		resultados[j] = newton_e(alfa,x0);
		gettimeofday(&t2, NULL);
		tiempos[j] = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
		
	}
	
	guardarTest(iteraciones,resultados,tiempos,8,"test_iteraciones_e_newton_a_4_x_01.txt");
	
	x0 = 0.5;
	alfa = 0.25;
	for (j = 0; j < 8; j++){
		
		cant_iteraciones = iteraciones[j];
		gettimeofday(&t1,NULL);
		resultados[j] = newton_e(alfa,x0);
		gettimeofday(&t2, NULL);
		tiempos[j] = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
		
	}
	
	guardarTest(iteraciones,resultados,tiempos,8,"test_iteraciones_e_newton_a_025_x_05.txt");
	
}

void test_iteraciones_e_secante(){

	int j;
	double iteraciones[8] = {20, 15, 10, 8, 5, 4, 3, 2};
	double tiempos[8];
	double resultados[8];
	
	double x0 = 0.1;
	double x1 = 0.2;
	double alfa = 4;
	criterio = "i";
	

	for (j = 0; j < 8; j++){
		
		cant_iteraciones = iteraciones[j];
		gettimeofday(&t1,NULL);
		resultados[j] = secante_e(alfa,x0,x1);
		gettimeofday(&t2, NULL);
		tiempos[j] = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
		
	}
	
	guardarTest(iteraciones,resultados,tiempos,8,"test_iteraciones_e_secante_a_4_x0_01_x1_02.txt");
	
	x0 = 0.5;
	x1 = 0.45;
	alfa = 0.25;
	for (j = 0; j < 8; j++){
		
		cant_iteraciones = iteraciones[j];
		gettimeofday(&t1,NULL);
		resultados[j] = secante_e(alfa,x0,x1);
		gettimeofday(&t2, NULL);
		tiempos[j] = (1000000*(t2.tv_sec-t1.tv_sec)+(t2.tv_usec-t1.tv_usec))/1000000.0;
		
	}
	
	guardarTest(iteraciones,resultados,tiempos,8,"test_iteraciones_e_secante_a_025_x0_05_x1_045.txt");
	
}



void guardarArreglo(double* arreglo, int n, const char* nombre){
	
	ofstream salida;
	
	salida.open(nombre);
	salida << n << endl;
	
	for(int i = 0; i < n; i++){
		salida << arreglo[i] << " ";
	}
	
	salida << endl;
	salida.close();
	
}

void guardarMatriz(double* matriz, int filas, int columnas, const char* nombre){
	
	ofstream salida;
	
	salida.open(nombre);
	salida << filas << " " << columnas << endl;
	for (int j = 0; j < filas; j++){
		for(int i = 0; i < columnas; i++){
			salida << matriz[j*columnas+i] << "\t";
		}
		salida << endl;
	}
	
	salida.close();
	
}

/* FIN TEST */
