///TP1 METODOS NUMERICOS 2C 2013
#include <iostream>
#include <fstream>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <cmath>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

using namespace std;

// cantidad de iteraciones es un criterio global de parada
// error absoluto = a
// diferencia de error = d

const char* 	criterio;
double 		epsilon;
double 		cant_iteraciones;
int		criterioAlcanzado = 0; 				///1: iteraciones, 2: diferencia, 3: error absoluto
timeval t1, t2;
double elapsedTime;




//Alcanzo iteraciones?	
bool alcanzoIteraciones(int iteraciones){
 	if (iteraciones >= cant_iteraciones) {
		criterioAlcanzado=1;
		return true;
	}
	return false;
}

//Alcanzo Diferencia?
bool alcanzoDiferencia(double anterior, double actual){
 	if (abs(actual - anterior) <= epsilon){
		criterioAlcanzado=2;
		return true;
	}
 	return false;
}

//Alcanzo Error Absoluto?
bool alcanzoErrorAbsoluto(double anterior, double actual){
 	if  ((abs(actual - anterior)/ abs(anterior)) <= epsilon){
			criterioAlcanzado=3;
			return true;
		}
	return false;
}

//Criterios de Parada    || alcanzoIteraciones(iteracion)
bool chequearParada(double anterior, double actual, int iteracion){

     if (strcmp(criterio, "a") ==  0){
		return (alcanzoErrorAbsoluto(anterior, actual));
     } else if (strcmp(criterio, "d") ==  0){
        	return (alcanzoDiferencia(anterior, actual)); 	
     } else if (strcmp(criterio, "i") ==  0){
        	return (alcanzoIteraciones(iteracion)); 	
     }
	 else {
		cout << "No se pudo interpretar el criterio de parada" << endl;
    }
}

// Funcion e(x)
double e(double x, double alpha){
	double aux=x*x;
	return (1/aux)-alpha;
}

// Derivada de e(x)
double e_derivada(double x){
	double aux=x*x*x;
	return (-2/aux);
}

// Metodo de la Secante para e(x)
double secante_e(double alpha,double x0, double x1){
    double xn = x1;
    double x_anterior = x0;
    double aux;
    int iteraciones = 0;
    while (!chequearParada(x_anterior, xn,iteraciones)) {    
        aux = (xn - x_anterior) / (e(xn,alpha) - e(x_anterior,alpha));
        x_anterior = xn;
        xn = xn - (e(xn,alpha) * aux);
        iteraciones++;
	}

    return xn;
}

// Funcion f(x)
double f(double x, double alpha){
        return x*x-alpha;
}

// Derivada de f(x)
double f_derivada(double x){
        return 2*x;
}

//Metodo de Newton para f(x)
double newton_f(double alpha, double x0){
	
        double anterior;
        double actual = x0;
        int iteraciones = 0;
        
        do{
		anterior=actual; 
                actual = actual - f(actual,alpha) * (1/f_derivada(actual));
                iteraciones ++;
        }while(!chequearParada(anterior, actual,iteraciones));
        
        return actual;
        
}

//Inversa de Newton de f(x)
double aprox_raiz_f(double alpha, double x0){
	return 1/newton_f(alpha,x0);
}

//Metodo de Newton para e(x)
double newton_e(double alpha, double x0){
	
        double anterior;
        double actual = x0;
        int iteraciones = 0;
        
        do{        
			anterior=actual; 
            actual = actual - e(actual,alpha) * (1/e_derivada(actual));
            iteraciones ++;
        }while(!chequearParada(anterior, actual,iteraciones));
        
        return actual;
        
}


int main(int argc, char *argv[])
{
   	/*
	 * param 0 = nombre ejecutable (tp2)
	 * param 1 = funcion que se va a ejecutar
	 * param 2 = metodo que se va a ejecutar
	 * param 3 = alfa
	 * param 4 = x0 inicial
	 * param 5 = criterio de parada
	 * param 6 = valor del criterio de parada
	 * param 7 = cantidad de iteraciones
	 * param 8 = x1 para criterio secante
	 */
	printf("Metodos numericos, TP1\n");     
	
	char* funcion = (argv[1]);
	char* metodo = (argv[2]);
	double alfa = atof(argv[3]);
	double inicial = atof(argv[4]);
	criterio = (argv[5]);
	epsilon = atof(argv[6]);
	cant_iteraciones = atof(argv[7]);
	double x1 = atof(argv[8]);
	
	double estimado;

	if (strcmp(funcion, "f") ==  0) {

		/// Se ejecuta la funcion f
		cout << "Funcion Elegida: f(x)." << endl;
		cout << "Alfa: " << alfa << endl;
		cout << "Criterio de Parada: ";
		if (strcmp(criterio, "a") ==  0){
          		cout << "Error Absoluto. " << endl;
			cout << "Epsilon: " << epsilon << endl;

		} else if (strcmp(criterio, "d") ==  0){
          		cout << "Diferencia entre xn y xn-1" << endl; 
			cout << "Epsilon: " << epsilon << endl;

		}
		else if (strcmp(criterio, "i") ==  0){
          		cout << "Cantidad de iteraciones" << endl;
			cout << "Cantidad de Iteraciones Permitidas: " << cant_iteraciones << endl;
		}
		cout << "X0: " << inicial << endl;
		cout << "Metodo predeterminado para f(x): Newton" << endl;
		estimado = newton_f(alfa, inicial);
	} else {
		/// Se ejecuta la funcion e
		cout << "Funcion Elegida: e(x)." << endl;
		cout << "Alfa: " << alfa << endl;
		cout << "Epsilon: " << epsilon << endl;
		cout << "Cantidad de Iteraciones Permitidas: " << cant_iteraciones << endl;

		if (strcmp(criterio, "a") ==  0){
          		cout << "Error Absoluto. " << endl;
			cout << "Epsilon: " << epsilon << endl;

		} else if (strcmp(criterio, "d") ==  0){
          		cout << "Diferencia entre xn y xn-1" << endl; 
			cout << "Epsilon: " << epsilon << endl;

		}
		else if (strcmp(criterio, "i") ==  0){
          		cout << "Cantidad de iteraciones" << endl;
			cout << "Cantidad de Iteraciones Permitidas: " << cant_iteraciones << endl;
		}

		if (strcmp(metodo, "n") ==  0) {
			/// Se ejecuta el metodo de newton		
			cout << "Metodo Elegido: Newton." << endl;
			estimado = newton_e(alfa, inicial);
		}
		if (strcmp(metodo, "s") ==  0) {
			/// Se ejecuta el metodo de la secante
			cout << "Metodo Elegido: Secante." << endl;
			cout << "X0: " << inicial << endl;
			cout << "X1: " << x1 << endl;
			estimado = secante_e(alfa,inicial,x1);
		}
	}
	cout << "Raiz de: " << alfa << " : " << estimado << endl;
	cout << "El programa finalizo segun el criterio de: " ;
	  switch(criterioAlcanzado)
    {
    case 1: cout << "Cantidad de Iteraciones. " << endl;
        break; 
    case 2: cout << "Diferencia entre xn y xn-1 < epsilon. "  << endl;
        break;
    case 3: cout << "Error Absoluto. "  << endl;
        break;
    default:cout<< "Error en la finalizacion. " <<endl;
    }
    return 0;
}
