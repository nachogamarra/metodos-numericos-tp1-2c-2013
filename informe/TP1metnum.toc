\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\'on}{4}
\contentsline {subsection}{\numberline {1.1}Normalizaci\'on de Vectores}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{4}
\contentsline {subsection}{\numberline {1.2}Funciones para calcular la ra\'iz cuadrada}{4}
\contentsline {subsection}{\numberline {1.3}M\'etodo de Punto Fijo}{5}
\contentsline {subsection}{\numberline {1.4}M\'etodo de Newton}{6}
\contentsline {subsection}{\numberline {1.5}M\'etodo de la Secante}{7}
\contentsline {subsection}{\numberline {1.6}Representaci\'on de los N\'umeros}{7}
\contentsline {subsection}{\numberline {1.7}Errores de Representaci\'on}{7}
\contentsline {section}{\numberline {2}Desarrollo}{9}
\contentsline {subsection}{\numberline {2.1}Partiendo del Enunciado}{9}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{}{9}
\contentsline {subsection}{\numberline {2.2}Demostraci\'on de convergencia para $f(x)$}{9}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{1)}{9}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{2)}{9}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{3)}{10}
\contentsline {paragraph}{Al haberse cumplido estas 3 condiciones, por teorema, $g(x)$ converge a su \'unico punto fijo. Con $x \in [\sqrt {\alpha },x_0]$.}{10}
\contentsline {subsection}{\numberline {2.3}Criterio de Parada}{10}
\contentsline {subsection}{\numberline {2.4}Algoritmos}{11}
\contentsline {subsubsection}{\numberline {2.4.1}Implementaci\'on del Algoritmo de Newton}{11}
\contentsline {subsubsection}{\numberline {2.4.2}Implementaci\'on del Algoritmo de la Secante}{11}
\contentsline {subsection}{\numberline {2.5}Consideraciones para los Tests}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{11}
\contentsline {section}{\numberline {3}Resultados}{12}
\contentsline {subsection}{\numberline {3.1}Resultados de la funci\'on $f(x)$}{12}
\contentsline {subsubsection}{\numberline {3.1.1}Selecci\'on de Semilla}{12}
\contentsline {subsubsection}{\numberline {3.1.2}Selecci\'on de Cantidad de iteraciones}{12}
\contentsline {subsubsection}{\numberline {3.1.3}Selecci\'on de Error Absoluto}{13}
\contentsline {subsubsection}{\numberline {3.1.4}Selecci\'on de diferencia entre $X_n - X_{n-1}$}{13}
\contentsline {subsubsection}{\numberline {3.1.5}Resultados sobre convergencia para distintos valores de $\alpha $}{13}
\contentsline {subsection}{\numberline {3.2}Resultados de la funci\'on $e(x)$ para Newton}{15}
\contentsline {subsubsection}{\numberline {3.2.1}Selecci\'on de Cantidad de iteraciones}{15}
\contentsline {subsubsection}{\numberline {3.2.2}Selecci\'on de Error Absoluto}{15}
\contentsline {subsubsection}{\numberline {3.2.3}Selecci\'on de diferencia entre $X_n - X_{n-1}$}{15}
\contentsline {subsubsection}{\numberline {3.2.4}Resultados sobre convergencia para distintos valores de $x_0$}{15}
\contentsline {paragraph}{1)}{15}
\contentsline {paragraph}{2)}{16}
\contentsline {paragraph}{3)}{16}
\contentsline {paragraph}{4)}{16}
\contentsline {paragraph}{}{16}
\contentsline {subsubsection}{\numberline {3.2.5}Resultados sobre convergencia para distintos valores de $\alpha $}{16}
\contentsline {subsection}{\numberline {3.3}Resultados de la funci\'on $e(x)$ para Secante}{18}
\contentsline {subsubsection}{\numberline {3.3.1}Selecci\'on de Cantidad de iteraciones}{18}
\contentsline {paragraph}{}{18}
\contentsline {subsubsection}{\numberline {3.3.2}Selecci\'on de Error Absoluto}{18}
\contentsline {subsubsection}{\numberline {3.3.3}Selecci\'on de diferencia entre $X_n - X{n-1}$}{18}
\contentsline {subsubsection}{\numberline {3.3.4}Resultados sobre convergencia para distintos valores de $x_0$ y $x_1$}{19}
\contentsline {paragraph}{1)}{19}
\contentsline {paragraph}{}{19}
\contentsline {paragraph}{2)}{19}
\contentsline {paragraph}{3)}{19}
\contentsline {paragraph}{4)}{20}
\contentsline {paragraph}{}{20}
\contentsline {subsubsection}{\numberline {3.3.5}Resultados sobre convergencia para distintos valores de $\alpha $}{20}
\contentsline {section}{\numberline {4}Discusi\'on}{22}
\contentsline {subsection}{\numberline {4.1}An\'alisis de la Funci\'on $f(x)$}{22}
\contentsline {subsection}{\numberline {4.2}An\'alisis de la funcion $e$ para Newton}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{}{24}
\contentsline {subsection}{\numberline {4.3}An\'alisis de la funcion $e$ para Secante}{24}
\contentsline {section}{\numberline {5}Conclusiones}{25}
\contentsline {subsection}{\numberline {5.1}Conclusiones sobre la Funci\'on $f(x)$}{25}
\contentsline {subsection}{\numberline {5.2}Conclusiones sobre la Funci\'on $e(x)$}{25}
\contentsline {subsubsection}{\numberline {5.2.1}Newton vs Secante}{25}
\contentsline {subsection}{\numberline {5.3}$f(x)$ vs $e(x)$}{25}
\contentsline {section}{\numberline {6}Ap\'endices}{27}
\contentsline {subsection}{\numberline {6.1}Ap\'endice A: Trabajo Pr\'actico 1}{27}
\contentsline {subsection}{\numberline {6.2}Ap\'endice B: C\'odigo}{29}
\contentsline {subsubsection}{\numberline {6.2.1}Implementaci\'on para calcular F}{29}
\contentsline {subsubsection}{\numberline {6.2.2}Implementaci\'on para calcular E utilizando Newton}{29}
\contentsline {subsubsection}{\numberline {6.2.3}Implementaci\'on para calcular E utilizando Secante}{30}
\contentsline {subsubsection}{\numberline {6.2.4}Criterio de Parada}{30}
\contentsline {subsubsection}{\numberline {6.2.5}Medici\'on de Tiempos}{31}
\contentsline {subsection}{\numberline {6.3}Ap\'endice I : Manual de uso}{32}
\contentsline {subsubsection}{\numberline {6.3.1}Compilaci\'on}{32}
\contentsline {subsubsection}{\numberline {6.3.2}Par\'ametros}{32}
\contentsline {subsection}{\numberline {6.4}Bibliografia}{33}
